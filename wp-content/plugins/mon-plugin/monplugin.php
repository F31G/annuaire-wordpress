<?php
/*
Plugin Name: Mon plugin 'annuaire'
Plugin URI: http://annuairewordpress.localhost
Description: Ceci est un plugin permettant de récupérer les données de la table 'annuaire' dans notre BDD en local
Author: Florian - Simplon.co
Version: 1.0
Author URI: http://annuairewordpress.localhost
*/

//  nous définissons notre [shortcode]
add_shortcode('annuaire', 'shortcode_annuaire');

// nous construisons notre fonction
function shortcode_annuaire() {
    global $wpdb;
    // on récupere * les données depuis mySql en initialisant la variable '$results'
    $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}annuaire", OBJECT );
// définir la variable '$liste'
$liste="<p>Ceci est la liste de vos contacts pro </p>";
// on retourne chaque valeurs du tableau '$result' en les initialisant dans '$entreprise'
        foreach($results as $entreprise){
            $liste.= "<article><p>{$entreprise -> id }</p>
            <h3>Entreprise : {$entreprise -> nom_entreprise}</h3>
            <h4>Localisation : {$entreprise -> localisation_entreprise}</h4>
            <h4>Contact : {$entreprise -> prenom_contact} {$entreprise -> nom_contact}, {$entreprise -> mail_contact}</h4></article>";
           
         }
         // on affiche la '$liste'
        return $liste;
    
// boucle infini :
//
//  return shortcode_annuaire();

}


// test pagination
 //posts_nav_link();

// Test pour voir qui est l'admin du serveur utilisé
//
// echo(exec("whoami"));
//

// Fonction et Shortcode de test
//
// add_action('wp_footer','recupAnnuaire');
//
// function recupAnnuaire(){
// echo('Faire Des Choses');
// }
